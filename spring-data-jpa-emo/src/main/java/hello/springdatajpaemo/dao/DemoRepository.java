package hello.springdatajpaemo.dao;

import hello.springdatajpaemo.entity.Student;
import org.springframework.data.repository.Repository;

public interface DemoRepository extends Repository<Student, Long> {
}
