package hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import hello.service.MessageService;

@Controller
public class LoginController {
	
	@Autowired
	private MessageService messageService;

	@RequestMapping("/login")
	public String login() {
		System.out.println("hello mvc");
		String mess = messageService.getMessage();
		System.out.println(mess);
		return "login";
	}
}
